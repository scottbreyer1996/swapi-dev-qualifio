// Write you code here

const swapi = require('swapi-node');
let userInput = "";

function promptPlanetsAndMovieInfos(movie, planets) {
    let totalDiameter = 0;
    let planetsRequired = [];

    for (const planet of planets) {
        if (planet.terrain.includes('mountains') && planet.surface_water > parseInt(0)) {
            totalDiameter += parseInt(planet.diameter);
            planetsRequired.push(planet);
        }
    }
    console.log('\nIn Film #' + userInput + ' (' + movie.title + ') there are ' + planetsRequired.length + ' planets that have mountains and a water surface (> 0).\n');

    for(const planet of planetsRequired) {
        console.log('- ' + planet.name + ', diameter: ' + planet.diameter);
    }
    console.log('\nTotal diameter: ' + totalDiameter + '\n');
}

async function getRessourcesByUrl(url) {
    return await swapi.get(url);
}

async function getMovie(movie) {
    return await swapi.films({id: movie})
}

async function main() {

    const prompt = require('prompt-sync')({sigint: true});
    let newInput = true;
    let movieChoosed;

    while (newInput) {

        userInput = prompt('\nChoose a Star Wars movie (1-6) : ');
        userInput = Number(userInput);

        if(userInput > 0 && userInput < 7) {
            let planetUrls = null;
            let planets = [];
            await getMovie(userInput).then((result) => {
                movieChoosed = result;
                planetUrls = result.planets;
            });
            for (let url of planetUrls) {
                await getRessourcesByUrl(url).then((result) => {
                    planets.push(result)
                });
            }
            promptPlanetsAndMovieInfos(movieChoosed, planets);

            userInput = prompt('Will you try again? (y/n) : ');

            if(userInput === 'n') {
                newInput = false;
                console.log('Bye!');
            }
        } else {
            console.log('Please enter a correct movie number');
        }
    }
}

main();
